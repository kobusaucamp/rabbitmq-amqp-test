package org.gertyes.amqp.test.rabbitmqamqptest.test2;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile({ "test2", "work-queues" })
@Configuration
public class Test2Config {

    @Bean
    public Queue hello() {
        return new Queue("hello");
    }

    @Profile("receiver")
    private static class ReceiverConfig {

        @Bean
        public Test2Receiver receiver1() {
            return new Test2Receiver(1);
        }

        @Bean
        public Test2Receiver receiver2() {
            return new Test2Receiver(2);
        }
    }

    @Profile("sender")
    @Bean
    public Test2Sender sender() {
        return new Test2Sender();
    }

}
